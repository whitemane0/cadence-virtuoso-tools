**Cadence Virtuoso Skill Scripts for Analog Layout Designers**

## Installation and execution

Download a script from this repository and place it in your skill scripts directory

in Cadence CIW (Virtuoso Main Window) type 

> load("/path_to_script/filename.il")

## labelpins.il - automatically generates and place label on all sected pin in pin's LPP